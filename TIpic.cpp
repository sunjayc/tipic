// TIpic.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "TIpic.h"
#include "stdio.h"

#define MAX_LOADSTRING 100

#define PIX (PIX_SIZE+1)
#define PIC_SIZE 1024
#define SHIFT_VAL 10

// Global Variables:
HINSTANCE hInst;								// current instance
TCHAR szTitle[MAX_LOADSTRING];					// The title bar text
TCHAR szWindowClass[MAX_LOADSTRING];			// the main window class name

int PIX_SIZE;
char pix[PIC_SIZE][PIC_SIZE];

// Forward declarations of functions included in this code module:
ATOM				MyRegisterClass(HINSTANCE hInstance);
BOOL				InitInstance(HINSTANCE, int);
LRESULT CALLBACK	WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK	About(HWND, UINT, WPARAM, LPARAM);

void				DrawPix(HDC hdc, int px, int py);

int APIENTRY _tWinMain(HINSTANCE hInstance,
                     HINSTANCE hPrevInstance,
                     LPTSTR    lpCmdLine,
                     int       nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

	PIX_SIZE = 4;

 	// TODO: Place code here.
	MSG msg;
	HACCEL hAccelTable;

	// Initialize global strings
	LoadString(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
	LoadString(hInstance, IDC_TIPIC, szWindowClass, MAX_LOADSTRING);
	MyRegisterClass(hInstance);

	// Perform application initialization:
	if (!InitInstance (hInstance, nCmdShow))
	{
		return FALSE;
	}

	hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_TIPIC));

	// Main message loop:
	while (GetMessage(&msg, NULL, 0, 0))
	{
		if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	return (int) msg.wParam;
}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
//  COMMENTS:
//
//    This function and its usage are only necessary if you want this code
//    to be compatible with Win32 systems prior to the 'RegisterClassEx'
//    function that was added to Windows 95. It is important to call this function
//    so that the application will get 'well formed' small icons associated
//    with it.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEX wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style			= 0;
	wcex.lpfnWndProc	= WndProc;
	wcex.cbClsExtra		= 0;
	wcex.cbWndExtra		= 0;
	wcex.hInstance		= hInstance;
	wcex.hIcon			= LoadIcon(hInstance, MAKEINTRESOURCE(IDI_TIPIC));
	wcex.hCursor		= LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground	= (HBRUSH)(COLOR_WINDOW+1);
	wcex.lpszMenuName	= MAKEINTRESOURCE(IDC_TIPIC);
	wcex.lpszClassName	= szWindowClass;
	wcex.hIconSm		= LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

	return RegisterClassEx(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   HWND hWnd;

   hInst = hInstance; // Store instance handle in our global variable

   hWnd = CreateWindow(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
      CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, NULL, NULL, hInstance, NULL);

   if (!hWnd)
   {
      return FALSE;
   }

   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return TRUE;
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND	- process the application menu
//  WM_PAINT	- Paint the main window
//  WM_DESTROY	- post a quit message and return
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	int wmId, wmEvent;
	PAINTSTRUCT ps;
	HDC hdc;
	static int xcur = 0;
	static int ycur = 0;
	static int shift = 1;
	RECT rect;
	static char buf[1024] = "";
	static int buflen = 0;
	char c;
	static bool coalesce = false;
	static bool timode = false;
	static HPEN sep = CreatePen(PS_SOLID, 0, RGB(128, 128, 128));
	static HBRUSH dot = (HBRUSH)GetStockObject(BLACK_BRUSH);
	static HBRUSH tiback = CreateSolidBrush(RGB(151, 164, 130));
	static HBRUSH whiteback = (HBRUSH)(COLOR_WINDOW+1);

	switch (message)
	{
	case WM_COMMAND:
		wmId    = LOWORD(wParam);
		wmEvent = HIWORD(wParam);
		// Parse the menu selections:
		switch (wmId)
		{
		case ID_FILE_TIMODE:
			InvalidateRect(hWnd, NULL, TRUE);
			if(!timode)
			{
				timode = true;
				CheckMenuItem(GetMenu(hWnd), wmId, MF_CHECKED);
				SetClassLongPtr(hWnd, GCLP_HBRBACKGROUND, (LONG_PTR)tiback);
				DeleteObject(sep);
				sep = CreatePen(PS_SOLID, 0, RGB(158, 171, 136));
				dot = CreateSolidBrush(RGB(55, 59, 47));
			}
			else
			{
				timode = false;
				CheckMenuItem(GetMenu(hWnd), wmId, MF_UNCHECKED);
				SetClassLongPtr(hWnd, GCLP_HBRBACKGROUND, (LONG_PTR)whiteback);
				DeleteObject(sep);
				DeleteObject(dot);
				sep = CreatePen(PS_SOLID, 0, RGB(128, 128, 128));
				dot = (HBRUSH)GetStockObject(BLACK_BRUSH);
			}
			break;
		case IDM_ABOUT:
			DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
			break;
		case IDM_EXIT:
			DestroyWindow(hWnd);
			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
		}
		break;
	case WM_LBUTTONDOWN:
	case WM_MOUSEMOVE:
		if(wParam & MK_LBUTTON)
		{
			if(!coalesce)
			{
				rect.left = xcur * PIX;
				rect.top = ycur * PIX;
				rect.right = xcur*PIX + PIX + 1;
				rect.bottom = ycur*PIX + PIX + 1;
				InvalidateRect(hWnd, &rect, TRUE);
				xcur = LOWORD(lParam) / PIX;
				ycur = HIWORD(lParam) / PIX;
				rect.left = xcur * PIX;
				rect.top = ycur * PIX;
				rect.right = xcur*PIX + PIX + 1;
				rect.bottom = ycur*PIX + PIX + 1;
				InvalidateRect(hWnd, &rect, TRUE);
				if(wParam & MK_CONTROL)
				{
					pix[xcur][ycur] = 1;
					buflen = 0;
				}
				else if(wParam & MK_SHIFT)
				{
					pix[xcur][ycur] = 0;
					buflen = 0;
				}
			}
		}
		break;
	case WM_CHAR:
		c = (char)wParam;
		if((c >= 'A' && c <= 'Z') || (c >= '0' && c <= '9') || (c >= 'a' && c <= 'z'))
		{
			buf[buflen++] = c;
			buf[buflen] = '\0';
		}
		else if(c == 0x0d && shift == SHIFT_VAL && buflen > 0) // Shift + Enter
		{
			int i, j;
			FILE* fp;
			strcat_s(buf, 1024, ".ylf");
			fopen_s(&fp, buf, "wb");
			if(fp != NULL)
			{
				for(i = 0; i < PIC_SIZE; i++)
				{
					for(j = 0; j < PIC_SIZE; j++)
					{
						if(pix[i][j])
						{
							fwrite(&i, sizeof(int), 1, fp);
							fwrite(&j, sizeof(int), 1, fp);
						}
					}
				}
				fclose(fp);
			}
			buflen = 0;
		}
		else if((c == 0x0d || c == ' ') && buflen > 0) // Enter or Spacebar
		{
			int i, j;
			FILE* fp;
			strcat_s(buf, 1024, ".ylf");
			fopen_s(&fp, buf, "rb");
			if(fp != NULL)
			{
				ZeroMemory(pix, PIC_SIZE * PIC_SIZE);
				while(!feof(fp))
				{
					fread(&i, sizeof(int), 1, fp);
					fread(&j, sizeof(int), 1, fp);
					if(!feof(fp))
						pix[i][j] = 1;
				}
				fclose(fp);
				InvalidateRect(hWnd, NULL, TRUE);
			}
			buflen = 0;
		}
		break;
	case WM_KEYDOWN:
		if(wParam == VK_UP || wParam == VK_DOWN || wParam == VK_LEFT || wParam == VK_RIGHT)
		{
			if(!coalesce)
			{
				rect.left = xcur * PIX;
				rect.top = ycur * PIX;
				rect.right = xcur*PIX + PIX + 1;
				rect.bottom = ycur*PIX + PIX + 1;
				InvalidateRect(hWnd, &rect, TRUE);
				switch(wParam)
				{
				case VK_UP:
					if(ycur > (shift - 1))
						ycur -= shift;
					break;
				case VK_DOWN:
					if(ycur < PIC_SIZE - shift - 1)
						ycur += shift;
					break;
				case VK_LEFT:
					if(xcur > (shift - 1))
						xcur -= shift;
					break;
				case VK_RIGHT:
					if(xcur < PIC_SIZE - shift - 1)
						xcur += shift;
					break;
				}
				rect.left = xcur * PIX;
				rect.top = ycur * PIX;
				rect.right = xcur*PIX + PIX + 1;
				rect.bottom = ycur*PIX + PIX + 1;
				InvalidateRect(hWnd, &rect, TRUE);
				buflen = 0;
			}
		}
		else if(wParam == VK_SHIFT)
			shift = SHIFT_VAL;
		else if(wParam == VK_SPACE || wParam == VK_RETURN || wParam == VK_DELETE)
		{
			if(!coalesce)
			{
				rect.left = xcur * PIX;
				rect.top = ycur * PIX;
				rect.right = xcur*PIX + PIX;
				rect.bottom = ycur*PIX + PIX;
				InvalidateRect(hWnd, &rect, TRUE);
				switch(wParam)
				{
				case VK_SPACE:
					if(buflen == 0)
						pix[xcur][ycur] = !pix[xcur][ycur];
					break;
				case VK_RETURN:
					if(buflen == 0)
						pix[xcur][ycur] = 1;
					break;
				case VK_DELETE:
					pix[xcur][ycur] = 0;
					buflen = 0;
					break;
				}
			}
		}
		else if(wParam == VK_HOME)
		{
			coalesce = false;
			InvalidateRect(hWnd, NULL, TRUE);
			buflen = 0;
		}
		else if(wParam == VK_END)
		{
			coalesce = true;
			InvalidateRect(hWnd, NULL, TRUE);
			buflen = 0;
		}
		else if(wParam == VK_PRIOR) // Page Up
		{
			PIX_SIZE++;
			InvalidateRect(hWnd, NULL, TRUE);
			buflen = 0;
		}
		else if(wParam == VK_NEXT) // Page Down
		{
			if(PIX_SIZE > 1)
			{
				PIX_SIZE--;
				InvalidateRect(hWnd, NULL, TRUE);
			}
			buflen = 0;
		}
		break;
	case WM_KEYUP:
		switch(wParam)
		{
		case VK_SHIFT:
			shift = 1;
			break;
		}
		break;
	case WM_PAINT:
		int i, j;
		hdc = BeginPaint(hWnd, &ps);

		if(coalesce)
		{
			SelectObject(hdc, GetStockObject(NULL_PEN));
			SelectObject(hdc, dot);
			for(i = 0; i < PIC_SIZE; i++)
			{
				for(j = 0; j < PIC_SIZE; j++)
				{
					if(pix[i][j])
					{
						Rectangle(hdc, i*PIX_SIZE, j*PIX_SIZE, i*PIX_SIZE + PIX_SIZE + 1, j*PIX_SIZE + PIX_SIZE + 1);
					}
				}
			}
		}
		else
		{
			SelectObject(hdc, sep);
			for(i = ps.rcPaint.left; i <= ps.rcPaint.right; i++)
			{
				if(i % PIX == 0)
				{
					MoveToEx(hdc, i, ps.rcPaint.top, NULL);
					LineTo(hdc, i, ps.rcPaint.bottom);
				}
			}
			for(j = ps.rcPaint.top; j <= ps.rcPaint.bottom; j++)
			{
				if(j % PIX == 0)
				{
					MoveToEx(hdc, ps.rcPaint.left, j, NULL);
					LineTo(hdc, ps.rcPaint.right, j);
				}
			}

			// pixels
			SelectObject(hdc, GetStockObject(NULL_PEN));
			SelectObject(hdc, dot);
			for(i = ps.rcPaint.left; i <= ps.rcPaint.right; i += PIX)
			{
				for(j = ps.rcPaint.top; j <= ps.rcPaint.bottom; j += PIX)
				{
					if(pix[i/PIX][j/PIX])
					{
						DrawPix(hdc, i/PIX, j/PIX);
					}
				}
			}
			
			// cursor
			SelectObject(hdc, GetStockObject(NULL_BRUSH));
			SelectObject(hdc, GetStockObject(BLACK_PEN));
			Rectangle(hdc, xcur*PIX, ycur*PIX, xcur*PIX + PIX + 1, ycur*PIX + PIX + 1);
		}

		EndPaint(hWnd, &ps);
		break;
	case WM_DESTROY:
		DeleteObject(tiback);
		DeleteObject(sep);
		if(timode)
			DeleteObject(dot);
		PostQuitMessage(0);
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}

// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
		{
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		break;
	}
	return (INT_PTR)FALSE;
}

void DrawPix(HDC hdc, int px, int py)
{
	Rectangle(hdc, px*PIX + 1, py*PIX + 1, px*PIX + PIX + 1, py*PIX + PIX + 1);
}
