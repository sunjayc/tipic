TI-style controls:
  Arrow keys	Move position
  Spacebar	PixelChange
  Enter		PixelOn
  Delete	PixelOff

Mouse controls:
  Left-click	Move position
  Ctrl+click	PixelOn
  Shift+click	PixelOff
  Note: All mouse controls work with click+drag as well

To save/load files:
  Type the filename (alphanumeric, no spaces) then hit:
    Space/Enter: load file
    Shift+Enter: save file
  Note: .ylf extension is automatically added when saving/loading

Toggle TI Mode (color changer):
  File -> TI mode
  Ctrl+T
